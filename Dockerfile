FROM ccr.ccs.tencentyun.com/tsf_100006412650/python_full_stack:v0.0.1
WORKDIR /root
COPY app.py /root
# COPY run.sh /root/python_full_stack
# GMT+8 for CentOS
RUN /bin/cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
RUN echo "Asia/Shanghai" > /etc/timezone
# run.sh
# CMD ["sh", "-c", "cd /root/python_full_stack; sh run.sh"]
CMD ["sh", "-c", "cd /root; python3 app.py"]